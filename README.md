gae-purchase-order-system ![image](https://travis-ci.org/gholts/gae-purchase-order-system.svg?branch=master)
=====================

A purchase order system that runs on Google's App Engine, Python flavour. It uses Google apps authentication, and it requires a custom apps domain set in the app settings.

This isn't fully "whitelabel" yet (or at all), as it has many specific settings to the setup I did for my church. It'd be easy to customize the template to your own needs and to use the SETTINGS.py file to setup your own admins and such though!
